;; -*- eval: (outline-hide-sublevels 6) -*-

;;; Packages

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
;             '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-hook 'dired-load-hook (lambda () (load "dired-x")))
(package-initialize)

;;; Custom

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auto-revert-interval 1)
 '(auto-save-file-name-transforms (quote ((".*" "~/.emacs.d/backup/" t))))
 '(backup-by-copying t)
 '(backup-directory-alist (quote (("." . "~/.emacs.d/backup/"))))
 '(column-number-mode t)
 '(custom-buffer-done-kill t)
 '(custom-enabled-themes (quote (doom-tomorrow-night)))
 '(custom-safe-themes
   (quote
    ("8aca557e9a17174d8f847fb02870cb2bb67f3b6e808e46c0e54a44e3e18e1020" default)))
 '(delete-old-versions t)
 '(dired-listing-switches "-lh")
 '(elpy-get-info-from-shell t)
 '(elpy-remove-modeline-lighter t)
 '(elpy-shell-echo-input-lines-head 3)
 '(elpy-shell-echo-input-lines-tail 3)
 '(emms-info-functions (quote (emms-info-libtag)))
 '(emms-player-list (quote (emms-player-mpv)))
 '(emms-source-file-default-directory "~/music")
 '(erc-nick "mmplx")
 '(erc-nick-uniquifier "_")
 '(fill-column 80)
 '(fit-window-to-buffer-horizontally t)
 '(flymake-error-bitmap (quote (exclamation-mark compilation-error)))
 '(global-company-mode t)
 '(gnus-secondary-select-methods (quote ((nntp "news.gmane.org"))))
 '(gnus-select-method
   (quote
    (nnimap "gmail"
            (nnimap-address "imap.gmail.com")
            (nnimap-server-port "imaps")
            (nnimap-stream ssl))))
 '(ido-default-buffer-method (quote selected-window))
 '(ido-enable-flex-matching t)
 '(ido-everywhere t)
 '(ido-mode (quote both) nil (ido))
 '(ido-ubiquitous-mode t)
 '(indent-tabs-mode nil)
 '(inhibit-startup-screen t)
 '(markdown-command "markdown_py -x extra")
 '(markdown-fontify-code-blocks-natively t)
 '(markdown-split-window-direction (quote right))
 '(menu-bar-mode nil)
 '(org-agenda-files (quote ("gtd.org" "inbox.org" "phone.org" "tablet.org")))
 '(org-babel-load-languages (quote ((emacs-lisp . t) (python . t))))
 '(org-babel-python-command "ipython -i --simple-prompt")
 '(org-beamer-environments-extra
   (quote
    (("proposition" "P" "\\begin{proposition}%a[%h]" "\\end{proposition}"))))
 '(org-confirm-babel-evaluate nil)
 '(org-cycle-separator-lines 1)
 '(org-directory "~/notes")
 '(org-export-backends (quote (ascii beamer html latex md)))
 '(org-footnote-auto-adjust t)
 '(org-highlight-latex-and-related (quote (latex)))
 '(org-latex-packages-alist (quote (("" "custom" t))))
 '(org-latex-pdf-process
   (quote
    ("time latexmk -outdir=/tmp/latexmk -f -pdf %f; mv /tmp/latexmk/%b.pdf .")))
 '(org-noter-always-create-frame nil)
 '(org-preview-latex-image-directory "/tmp/ltximg/")
 '(org-refile-use-outline-path t)
 '(org-stuck-projects (quote ("-someday+LEVEL=2/-DONE" ("TODO") nil "")))
 '(package-selected-packages
   (quote
    (emms projectile visual-fill-column magit pdf-tools xclip org-noter ido-completing-read+ markdown-mode elpy doom-themes)))
 '(pdf-view-midnight-colors (quote ("#c5c8c6" . "#1d1f21")))
 '(pdf-view-resize-factor 1.07)
 '(projectile-dynamic-mode-line nil)
 '(python-check-command "flake8")
 '(python-flymake-command (quote ("flake8" "-")))
 '(python-shell-interpreter "ipython")
 '(python-shell-interpreter-args "-i --simple-prompt")
 '(ring-bell-function (quote doom-themes-visual-bell-fn))
 '(safe-local-variable-values
   (quote
    ((eval outline-hide-sublevels 6))))
 '(send-mail-function (quote smtpmail-send-it))
 '(set-mark-command-repeat-pop t)
 '(show-paren-mode t)
 '(shr-color-visible-luminance-min 68)
 '(smtpmail-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-service 25)
 '(split-width-threshold 150)
 '(sql-interactive-mode-hook (quote (toggle-truncate-lines)))
 '(sql-postgres-options (quote ("-P" "pager=off" "-e")))
 '(tab-width 4)
 '(text-scale-mode-step 1.05)
 '(tooltip-frame-parameters
   (quote
    ((name . "tooltip")
     (internal-border-width . 5)
     (border-width . 1)
     (no-special-glyphs . t)
     (font-backend . "xft"))))
 '(truncate-partial-width-windows t)
 '(use-dialog-box nil)
 '(user-full-name "Carlos Pita")
 '(user-mail-address "carlosjosepita@gmail.com")
 '(version-control t)
 '(visual-line-fringe-indicators (quote (left-curly-arrow right-curly-arrow)))
 '(winner-mode t)
 '(x-gtk-use-system-tooltips nil)
 '(xclip-mode t))

;;; Theme

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(Info-quoted ((t (:slant italic))))
 '(company-tooltip ((t (:inherit default))))
 '(custom-state ((t (:background "#1d1f21"))))
 '(doom-modeline-error ((t (:background "#cc6666" :foreground "#1d1f21"))))
 '(emms-playlist-selected-face ((t (:foreground "#81a2bd"))))
 '(emms-playlist-track-face ((t nil)))
 '(erc-current-nick-face ((t (:foreground "#b5bd68" :weight bold))))
 '(erc-timestamp-face ((t (:foreground "#8abeb7"))))
 '(fringe ((t (:background "#1c1e20"))))
 '(line-number ((t (:height 0.85))))
 '(line-number-current-line ((t (:height 0.85))))
 '(mode-line ((t (:foreground "#cfd7d3" :background "#13191c"))))
 '(mode-line-inactive ((t (:foreground "#83898b" :background "#13191c"))))
 '(org-block ((t (:background "#1d1f21"))))
 '(org-block-begin-line ((t (:foreground "#5a5b5a" :background "#1d1f21"))))
 '(org-latex-and-related ((t (:foreground "#b294bb"))))
 '(region ((t (:background "#282a2e"))))
 '(secondary-selection ((t (:background "#414241"))))
 '(tooltip ((t (:background "#292b2b" :foreground "#c5c8c6" :height 100 :family "DejaVu Sans"))))
 '(vertical-border ((t (:foreground "#101619" :background "#101619"))))
 '(widget-field ((t (:background "#282a2e")))))

(add-hook 'Info-mode-hook
          (lambda ()
            (face-remap-set-base 'default :family "DejaVu Sans Mono")))

(defun my-lightweight-outline ()
    (dolist (face '(org-level-1 outline-1
                    org-level-2 outline-2
                    org-level-3 outline-3
                    org-level-4 outline-4
                    org-level-5 outline-5))
      (face-remap-add-relative face :inherit 'default)))

(doom-themes-org-config)

;;; Common

;;;; Base modes

(require 'whitespace)
(require 'hideshow)
(require 'outline)

(defun my-common-init ()
  (when (and buffer-file-name (not buffer-read-only))
    (setq-local whitespace-style '(face trailing))
    (my-fill-mode)
    (whitespace-mode)))

(defun my-prog-mode-hook ()
  (my-common-init)
  (flymake-mode)
  (hs-minor-mode)
  (outline-minor-mode))

(defun my-text-mode-hook()
  (my-common-init))

(add-hook 'prog-mode-hook #'my-prog-mode-hook)
(add-hook 'text-mode-hook #'my-text-mode-hook)

;;;; Global keys

(defun my-simplify-fold-keymap (mode)
  (let ((map (make-sparse-keymap)))
     (map-keymap (lambda (ev def)
                   (define-key map (string (event-basic-type ev)) def))
                 (lookup-key
                  (alist-get mode minor-mode-map-alist)
                  (kbd "C-c @")))
     map))

(defvar my-keymap (make-sparse-keymap))
(dolist (bind `(("c" . (make-sparse-xkeymap))
                ("e" . my-emms)
                ("f" . flymake-mode)
                ("g" . magit-status)
                ("l" . display-line-numbers-mode)
                ("m" . imenu)
                ("n" . org-noter)
                ("o" . (lambda () (interactive)
                         (switch-to-buffer (other-buffer))))
                ("p" . pyvenv-activate)
                ("r" . revert-buffer)
                ("R" . auto-revert-mode)
                ("s" . flyspell-mode)
                ("w" . my-fill-mode)
                ("x" . ,(my-simplify-fold-keymap #'hs-minor-mode))
                ("x x" . hs-toggle-hiding)
                ("z" . ,(my-simplify-fold-keymap #'outline-minor-mode))
                ("z z" . outline-toggle-children)))
  (define-key my-keymap (kbd (car bind)) (cdr bind)))
(global-set-key (kbd "C-c") my-keymap)

(global-set-key (kbd "s-B") #'windmove-left)
(global-set-key (kbd "s-F") #'windmove-right)
(global-set-key (kbd "s-P") #'windmove-up)
(global-set-key (kbd "s-N") #'windmove-down)

;;;; Line wrapping

(defun my-toggle-lines-tail (on)
  ;; https://www.reddit.com/r/emacs/comments/9ktjx3
  (if (and on (not (eq major-mode 'org-mode)))
      (add-to-list 'whitespace-style 'lines-tail)
    (setq-local whitespace-style
          (delq 'lines-tail whitespace-style)))
  (when (and (boundp 'whitespace-mode) whitespace-mode)
    (whitespace-mode -1)
    (whitespace-mode)))

(define-minor-mode my-fill-mode nil nil nil nil
  (my-toggle-lines-tail my-fill-mode)
  (visual-line-mode (when my-fill-mode -1))
  (visual-fill-column-mode (when my-fill-mode -1))
  (auto-fill-mode (unless my-fill-mode -1)))

;; https://github.com/joostkremers/visual-fill-column#adjusting-text-size
(advice-add #'text-scale-adjust :after #'visual-fill-column-adjust)

;;;; Line numbering

(defun my-goto-line-numbers-advice (fun)
  (interactive)
  (unwind-protect
      (progn (display-line-numbers-mode 1)
             (call-interactively fun))
    (display-line-numbers-mode -1)))
(advice-add #'goto-line :around #'my-goto-line-numbers-advice)

;;;; Projectile

(require 'projectile)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
(projectile-mode +1)

;;;; Company

(global-set-key (kbd "C-<tab>") #'company-complete)

(eval-after-load 'company
  '(progn
     (define-key company-active-map (kbd "<tab>")
       'company-complete-common-or-cycle)
     (define-key company-active-map (kbd "<backtab>")
       'company-select-previous)
     (define-key company-active-map (kbd "M-<tab>")
       'company-complete)))

;;;; Modeline

(dolist (mode '(projectile-mode
                whitespace-mode
                hs-minor-mode
                outline-minor-mode
                auto-fill-function))
  (setcar (alist-get mode minor-mode-alist) ""))

(let ((mode (assq #'flymake-mode minor-mode-alist)))
  (setq minor-mode-alist (cons mode (remq mode minor-mode-alist))))

(defun my-flymake-modeline-advice (ret)
  (setf (cadar ret) " Fly") ret)
(advice-add #'flymake--mode-line-format
            :filter-return #'my-flymake-modeline-advice)

;;;; Misc

(setq frame-title-format "%b - Emacs")

(put 'narrow-to-region 'disabled nil)

;; Space matches anything https://www.reddit.com/r/emacs/comments/3yxk2x
(setq search-whitespace-regexp ".*?")

;;; Major modes

;;;; Emacs lisp

(defun my-emacs-lisp-mode-hook ()
  (setq tab-width 8))
(add-to-list 'emacs-lisp-mode-hook #'my-emacs-lisp-mode-hook)

;;;; Elpy

(defun my-elpy-mode-hook ()
  (setq fill-column 79))
(add-hook 'elpy-mode-hook #'my-elpy-mode-hook)

;; https://github.com/jorgenschaefer/elpy/issues/1114
(defun my-elpy-profiling-interpreter-advice (f &rest args)
  (let ((python-shell-interpreter "python")
        (python-shell-interpreter-args ""))
    (apply f args)))
(advice-add #'elpy-profile-buffer-or-region
            :around #'my-elpy-profiling-interpreter-advice)

;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=32344
;; https://github.com/jorgenschaefer/elpy/issues/1428
(defun my-elpy-highlight-input-advice
    (f string face &optional no-font-lock)
  (if (not (eq face 'comint-highlight-input))
      (funcall f string face no-font-lock)
    (funcall f string face t)
    (python-shell-font-lock-post-command-hook)))
(advice-add #'elpy-shell--insert-and-font-lock
            :around #'my-elpy-highlight-input-advice)

(defun my-comint-highlight-input-advice (f &rest args)
  (if (eq major-mode 'inferior-python-mode)
      (cl-letf* ((g (symbol-function 'add-text-properties))
                 ((symbol-function 'add-text-properties)
                  (lambda (start end properties &optional object)
                    (unless (eq (nth 3 properties) 'comint-highlight-input)
                     (funcall g start end properties object)))))
        (apply f args))
    (apply f args)))
(advice-add #'comint-send-input :around #'my-comint-highlight-input-advice)

(elpy-enable)

;; https://github.com/jorgenschaefer/elpy/issues/1399
(define-key elpy-mode-map (kbd "M-<tab>") #'company-capf)

;;;; Shell

(add-to-list 'auto-mode-alist '("PKGBUILD" . sh-mode))

(defun my-shell-mode-hook ()
  (face-remap-set-base 'comint-highlight-prompt :inherit nil))
(add-hook 'shell-mode-hook #'my-shell-mode-hook)

;;;; Org

(require 'org-protocol)
(require 'org-capture)
(require 'ox)

(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-switchb)

(setq org-capture-templates
      `(("i" "Inbox Note" entry (file "inbox.org")
         "* %?" :empty-lines 1)
        ("p" "Protocol Link" entry (file "inbox.org")
         "* %:description\n\n%:link\n\n%i%?" :empty-lines 1)
        ("L" "Protocol Link" entry (file "inbox.org")
         "* %:description\n\n%:link%?" :empty-lines 1)))

(setq org-refile-targets '((nil :maxlevel . 2)
                           (("gtd.org" "diario.org")
                            :maxlevel . 2)))

(setq org-todo-keywords
      '((sequence "TODO(t)" "SOMEDAY(s)" "|" "DONE(d)")))

(setq org-tag-alist '(("export" . ?e)))

(defun my-org-find-in-docs (tag)
  (concat "file:/"
          (substring (shell-command-to-string
                      (concat "find ~/archive/docs/ -iname '*"
                              tag
                              "*' | head -1"))
                     0 -1)))

(setq org-link-abbrev-alist
      '(("google" . "https://www.google.com/#q=")
        ("docs" . my-org-find-in-docs)))

(setq org-babel-default-header-args:python '((:session)))

(defun my-org-babel-redisplay-images ()
  (when org-inline-image-overlays
    (org-redisplay-inline-images)))
(add-hook 'org-babel-after-execute-hook
          #'my-org-babel-redisplay-images)

;; http://lists.gnu.org/archive/html/emacs-orgmode/2018-12/msg00002.html
;; https://www.reddit.com/r/emacs/comments/a27jqh
(defun my-org-special-block-filter (tree backend _symbol)
  (pcase-let ((`(,prop ,fmt)
               (cond ((org-export-derived-backend-p backend 'latex)
                      '(:attr_latex ":options %s")))))
    (when prop
      (org-element-map tree 'special-block
        (lambda (element)
          (unless (org-element-property prop element)
            (save-excursion
              (goto-char (org-element-property :begin element))
              (when (looking-at "[ \t]*#\\+begin_\\S-+[ \t]+\\([^\n]+\\)")
                (let ((attr (format fmt (match-string-no-properties 1))))
                  (org-element-put-property element prop (list attr))))))
          nil)))))
(add-to-list 'org-export-filter-parse-tree-functions
             #'my-org-special-block-filter)

(font-lock-add-keywords 'org-mode
  '(("\\(#\\+\\(BEGIN\\|begin\\|END\\|end\\)_\\(\\S-+\\)\\)\\([^\n:]*\\)"
     (1 '(:foreground "#5a5b5a" :background "#292b2b") t) ; directive
     (3 '(:foreground "#81a2be" :background "#292b2b") t) ; kind
     (4 '(:foreground "#c5c8c6") t))) ; title
  t)

(dolist (tmpl '(("d" "#+BEGIN_definition ?\n\n#+END_definition")
                ("p" "#+BEGIN_proposition ?\n\n#+END_proposition")
                ("P" "#+BEGIN_proof\n?\n#+END_proof")
                ("a" "#+BEGIN_alertblock {\faExclamationTriangle{}?}\n\n#+END_alertblock")
                ("b" "#+BEGIN_?\n\n#+END_")))
  (add-to-list 'org-structure-template-alist tmpl))

;; https://www.reddit.com/r/emacs/comments/9ggimh
(defun my-org-latex-preview-advice (beg end &rest _args)
    (let* ((ov (car (overlays-in beg end)))
           (img (cdr (overlay-get ov 'display)))
           (new-img (plist-put img :ascent 90)))
      (overlay-put ov 'display (cons 'image new-img))))
(advice-add #'org--format-latex-make-overlay
            :after #'my-org-latex-preview-advice)

(setq org-format-latex-options
      (plist-put org-format-latex-options :scale 1.3))

(defun my-org-mode-hook ()
  (let* ((in-goto (string= (buffer-name) "*org-goto*"))
         (in-notes (string-prefix-p (expand-file-name "~/notes/")
                                    buffer-file-name))
         (note-name (and in-notes (file-name-base buffer-file-name))))
    (when in-notes
      (org-indent-mode))
    (when (string= note-name "inbox")
      (auto-save-visited-mode))
    (when (or in-goto (member note-name '("inbox" "tablet" "phone")))
      (my-lightweight-outline))
    (dolist (bind `(("b" . org-beamer-export-to-pdf)
                    ("l" . org-latex-export-to-pdf)
                    ("e" . latex-insert-block)
                    ("o" . ,(kbd "C-x b org SPC output RET \
                                  M-< C-s Fatal SPC error"))))
      (local-set-key (kbd (concat "C-c c " (car bind))) (cdr bind)))
    (setq font-lock-extend-after-change-region-function
          #'my-org-extend-region)
    (add-hook 'completion-at-point-functions
              #'pcomplete-completions-at-point nil t)))
(add-hook 'org-mode-hook #'my-org-mode-hook)

;;;; Markdown

(defun my-markdown-mode-hook ()
  (when (string-suffix-p "README.md" buffer-file-name)
    (my-fill-mode -1)))
(add-hook 'markdown-mode-hook #'my-markdown-mode-hook)

;;;; Pdf-tools

(pdf-tools-install)
(add-hook 'pdf-view-mode-hook #'pdf-view-midnight-minor-mode)
(add-hook 'pdf-outline-buffer-mode-hook #'my-lightweight-outline)

;;;; EMMS

(defun my-emms (playlist)
  (interactive "P")
  (unless (featurep 'emms)
    (require 'emms-setup)
    (require 'emms-info-libtag)
    (emms-all))
  (cond ((equal major-mode 'dired-mode)
         (call-interactively 'emms-add-dired))
        ((or (null emms-playlist-buffer)
             (not (buffer-live-p emms-playlist-buffer))
             (eq (current-buffer) emms-playlist-buffer))
         (call-interactively
          (if playlist 'emms-add-playlist 'emms-add-directory-tree))))
  (emms-playlist-mode-go))